<div align="center">

<img src="https://cdn-icons-png.flaticon.com/512/6385/6385053.png" width="25%">

# Departamento de Infraestrutura

Este grupo tem a tarefa de gerenciar o armazenamento de todo o código e informações relacionadas aos nossos ambientes e à infraestrutura em geral. Dentro deste grupo, você encontrará abundante conteúdo sobre nossa infraestrutura na nuvem.

</div>


## Introdução

Este grupo criado no Gitlab tem como principal responsabilidade o armazenamento de códigos relacionados à nossa infraestrutura na nuvem. Dentro deste repositório, você encontrará uma variedade de códigos e implementações que utilizam tecnologias como Google Cloud, Terraform e outras ferramentas.

## Propósito

A equipe de Infraestrutura que atua dentro deste grupo assume a responsabilidade pela gestão da infraestrutura subjacente dos serviços que estão em execução dentro do âmbito dos projetos do grupo.

## Contribuidores

Este projeto é mantido com a colaboração de diversos membros que enviam solicitações de mesclagem. Abaixo está a lista dos principais colaboradores deste projeto:

- [Clique neste link para visualizar os colaboradores](https://gitlab.com/groups/snct-lagarto-2023/infrastructure-department/-/group_members)